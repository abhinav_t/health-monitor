package org.healthmonitor.model;

import org.cibilinfo.model.CIBILInformation;
import org.fininfo.model.FinancialInformation;

public class AssessmentInput {

    public String name;
    public String address;
    public Double monthlyIncome;
    public Double annualCTC;
    public Double typicalMonthlyExpenses;
    public Double lifeInsuranceCover;
    public Double medicalInsuranceCover;
    public String panCardNumber;
    public Integer cibilScore;

    public AssessmentInput(String name, String address, Double monthlyIncome, Double annualCTC,
                           Double typicalMonthlyExpenses, Double lifeInsuranceCover, Double medicalInsuranceCover,
                           String panCardNumber, Integer cibilScore) {
        this.name = name;
        this.address = address;
        this.monthlyIncome = monthlyIncome;
        this.annualCTC = annualCTC;
        this.typicalMonthlyExpenses = typicalMonthlyExpenses;
        this.lifeInsuranceCover = lifeInsuranceCover;
        this.medicalInsuranceCover = medicalInsuranceCover;
        this.panCardNumber = panCardNumber;
        this.cibilScore = cibilScore;
    }

    public static AssessmentInput buildUsing(FinancialInformation financialInformation,
                                             CIBILInformation cibilInformation) {
        return new AssessmentInput(financialInformation.name, financialInformation.address,
                financialInformation.monthlyIncome, financialInformation.annualCTC,
                financialInformation.typicalMonthlyExpenses, financialInformation.lifeInsuranceCover,
                financialInformation.medicalInsuranceCover, cibilInformation.panCardNumber,
                cibilInformation.cibilScore);
    }

}
