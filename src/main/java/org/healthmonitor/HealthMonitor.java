package org.healthmonitor;

import org.cibilinfo.fetcher.CIBILInformationFetcher;
import org.cibilinfo.model.CIBILInformation;
import org.fininfo.fetcher.FinancialInformationFetcher;
import org.fininfo.model.FinancialInformation;
import org.healthmonitor.model.AssessmentInput;
import org.healthmonitor.model.ExecutionResult;
import org.rules.runner.RulesRunner;

public class HealthMonitor {

    private final FinancialInformationFetcher financialInformationFetcher;
    private final CIBILInformationFetcher cibilInformationFetcher;
    private final RulesRunner rulesRunner;

    public HealthMonitor() {
        this.financialInformationFetcher = new FinancialInformationFetcher();
        this.cibilInformationFetcher = new CIBILInformationFetcher();
        this.rulesRunner = new RulesRunner();
    }

    private ExecutionResult execute(AssessmentInput assessmentInput, ExecutionResult healthScore) {
        rulesRunner.execute(assessmentInput, healthScore);
        return healthScore;
    }

    private AssessmentInput buildAssessmentInput(String customerId) {
        FinancialInformation financialInformation = financialInformationFetcher.getFinancialInformation(customerId);
        CIBILInformation cibilInformation = cibilInformationFetcher.getCIBILInformation(customerId);
        return AssessmentInput.buildUsing(financialInformation, cibilInformation);
    }

    public static void main(String[] args) {
        HealthMonitor healthMonitor = new HealthMonitor();
        AssessmentInput input = healthMonitor.buildAssessmentInput(mockCustomerId());
        ExecutionResult result = healthMonitor.execute(input, new ExecutionResult(0));
        System.out.println("Health Assessment Score = " + result.getScore());
    }

    private static String mockCustomerId() {
        return "007";
    }

}
